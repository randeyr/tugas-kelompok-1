from django.urls import path
from . import views

app_name = 'rating'

urlpatterns = [
    path('',views.halaman_form, name='halaman_form'),
    path('ratingwebkami/',views.halaman_rating,name='halaman_rating'),
    path('berhasil/',views.feedback_form,name='feedback_form'),
]

