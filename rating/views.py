from django.shortcuts import render
from .models import Akun

# Create your views here.

def halaman_form(request):

    return render(request,'halaman_form.html')

def feedback_form(request):
    args = {}
    username = request.POST['username'];
    rating = request.POST['inlineRadioOptions']

    user_rating = Akun(username=username,rating=rating)
    user_rating.save()
    tampilan = "Rating anda berhasil tersimpan"
    args['sukses'] = tampilan
    return render(request,'halaman_form.html',args)

def halaman_rating(request):
    info_rating = Akun.objects.all()
    rating = {
        "info_rating" : info_rating
    }
    return render(request,'halaman_rating.html',rating)

