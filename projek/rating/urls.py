from django.urls import path
from . import views

app_name = 'rating'

urlpatterns = [
    path('',views.halaman_signup, name='halaman_signup'),
    path('ratingwebkami/',views.halaman_rating,name='halaman_rating'),
    path('berhasil/',views.feedback_signup,name='feedback_signup'),
]

