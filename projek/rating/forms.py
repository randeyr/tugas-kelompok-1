from django import forms

class RatingForm(forms.Form):

    RATING = [
        ('1','1'),
        ('2','2'),
        ('3','3'),
        ('4','4'),
        ('5','5'),
    ]

    nama = forms.CharField(max_length=100)
    review = forms.ChoiceField(choices=RATING,widget=forms.RadioSelect)