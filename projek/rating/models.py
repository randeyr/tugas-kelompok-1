from django.db import models

# Create your models here.
class Akun(models.Model):
    username = models.CharField(max_length=200)
    RATING = (
        ('1','1'),
        ('2','2'),
        ('3','3'),
        ('4','4'),
        ('5','5'),
    )
    rating = models.CharField(max_length=10, choices=RATING,default='')