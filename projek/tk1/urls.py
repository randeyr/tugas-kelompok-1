from django.urls import path, include
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('covidsimulasi.urls')),
    path('rating/',include('rating.urls')),
    path('simulasi/', include('simulasi.urls'))
    
]