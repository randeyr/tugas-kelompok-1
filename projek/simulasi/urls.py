from django.urls import include, path
from .views import jumlah
from .views import savejumlah
from .views import savependuduk
from .views import form
from .views import daftarpositif
from .views import sembuhkan


urlpatterns = [
    path('', jumlah, name='jumlah'),
    path('jumlah',jumlah,name='jumlah'),
    path('savejumlah', savejumlah, name='savejumlah' ),
    path('form/savependuduk', savependuduk, name='savependuduk'),
    path('form/', form, name='form'),
    path('daftarpositif/', daftarpositif, name='daftarpositif'),
    path('daftarpositif/<int:id>', sembuhkan, name='sembuhkan')
]

