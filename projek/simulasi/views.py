from django.shortcuts import render
from .forms import FormPenduduk, FormJumlahPenduduk
from .models import Penduduk
from django.http import HttpResponseRedirect


# Create your views here.
def jumlah(request):
	formjumlah=FormJumlahPenduduk
	return render(request, 'jumlah.html',{'formjumlah':formjumlah})

def form(request):
	form=FormPenduduk
	return render(request, 'form.html',{'form':form})

def savejumlah(request):
	if request.method=='POST':
		formjumlah=FormJumlahPenduduk(request.POST)
		if formjumlah.is_valid():
			formjumlah.save()
			return HttpResponseRedirect('/simulasi/form')
	else:
		return HttpResponseRedirect('/simulasi/form')

def savependuduk(request):
	if request.method=='POST':
		form = FormPenduduk(request.POST)
		if form.is_valid():
			data = Penduduk()
			data.nama_penduduk = form.cleaned_data['nama_penduduk']
			data.alamat = form.cleaned_data['alamat']
			data.usia= form.cleaned_data['usia']
			data.jenis_kelamin = form.cleaned_data['jenis_kelamin']
			data.status_covid = form.cleaned_data['status_covid']
			data.save()
			
			return render(request, 'form.html', {'form' : form})
	else:
		form=FormPenduduk()
		return render(request, 'form.html', {'form' : form})

    

def sembuhkan(request,id):
    Penduduk.objects.filter(id=id).delete()
    penduduk = Penduduk.objects.all().filter(status_covid='Positif')
    return render(request, 'daftarpositif.html', {'positif':penduduk})


def daftarpositif(request):
	positif = Penduduk.objects.all().filter(status_covid='Positif')
	html = 'daftarpositif.html'
	return render(request, html, {'positif':positif})

