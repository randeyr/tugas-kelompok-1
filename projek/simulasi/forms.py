from django.forms import ModelForm
from  .models import Penduduk, JumlahPendudukAwal
from django import forms

class FormPenduduk(forms.ModelForm):
	class Meta:
		model = Penduduk
		fields = '__all__'
		widgets = {
            'jenis_kelamin': forms.Select,
            'status_covid': forms.Select,
        }
        				
class FormJumlahPenduduk(forms.ModelForm):
	class Meta:
		model = JumlahPendudukAwal
		fields = '__all__'
	error_messages = {
		'required' : 'Please Type'
	}