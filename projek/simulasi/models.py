from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class JumlahPendudukAwal(models.Model):
	jumlah=models.IntegerField(validators=[
            MaxValueValidator(999999),
            MinValueValidator(1)
        ])

class Penduduk(models.Model):
    nama_penduduk= models.CharField(max_length=25)
    usia = models.CharField(max_length=2)
    alamat = models.CharField(max_length=50)
    
    LK = 'Laki-Laki'
    PR = 'Perempuan'


    JENIS_KELAMIN = (
        (LK, 'Laki-Laki'),
        (PR, 'Perempuan'),
               )    
    jenis_kelamin = models.CharField(
        null=True, max_length=9,
        default=None, 
        choices=JENIS_KELAMIN, verbose_name='jenis kelamin ')

    
    Ps = 'Positif'
    Ng = 'Negatif'


    STATUS_COVID = (
        (Ps, 'Positif'),
        (Ng, 'Negatif'),
               )    
    status_covid = models.CharField(
        null=True, max_length=7,
        default=None, 
        choices=STATUS_COVID, verbose_name='status covid ')




